<?php

/**
 * @file
 * Editor integration functions for CodeMirror.
 */

/**
 * Implements hook_editor().
 */
function codemirror_editor_codemirror_editor() {
  $editor['codemirror'] = array(
    // The official, human-readable label of the editor library.
    'title' => 'CodeMirror',
    // The URL to the library's homepage.
    'vendor url' => 'http://codemirror.net',
    // The URL to the library's download page.
    'download url' => 'http://codemirror.net',
    'library path' => wysiwyg_get_path('codemirror'),
    // A definition of available variants for the editor library.
    // The first defined is used by default.
    'libraries' => array(
      '' => array(
        'title' => 'Default',
        'files' => array(
          'lib/codemirror.js' => array('preprocess' => FALSE),
          'addon/edit/matchbrackets.js' => array('preprocess' => FALSE),
          'addon/edit/closebrackets.js' => array('preprocess' => FALSE),
          'addon/fold/xml-fold.js' => array('preprocess' => FALSE),
          'addon/edit/matchtags.js' => array('preprocess' => FALSE),
          'addon/edit/trailingspace.js' => array('preprocess' => FALSE),
          'addon/edit/closetag.js' => array('preprocess' => FALSE),
          'addon/hint/show-hint.js' => array('preprocess' => FALSE),
          'addon/hint/xml-hint.js' => array('preprocess' => FALSE),
          'addon/hint/html-hint.js' => array('preprocess' => FALSE),
          'addon/selection/active-line.js' => array('preprocess' => FALSE),
          'mode/xml/xml.js' => array('preprocess' => FALSE),
          'mode/javascript/javascript.js' => array('preprocess' => FALSE),
          'mode/css/css.js' => array('preprocess' => FALSE),
          'mode/htmlmixed/htmlmixed.js' => array('preprocess' => FALSE),
          'mode/markdown/markdown.js' => array('preprocess' => FALSE),
          'mode/clike/clike.js' => array('preprocess' => FALSE),
          'mode/php/php.js' => array('preprocess' => FALSE),
        ),
      ),
    ),
    // (optional) A callback to invoke to return additional notes for
    // installing the editor library in the administrative list/overview.
    // 'install note callback' => 'wysiwyg_codemirror_install_note',
    // A callback to determine the library's version.
    'version callback' => 'codemirror_editor_version',
    // A callback to return available themes/skins for the editor library.
    'themes callback' => 'codemirror_editor_themes',
    // (optional) A callback to perform editor-specific adjustments or
    // enhancements for the administrative editor profile settings form.
    'settings form callback' => 'codemirror_editor_settings_form',
    // A callback to convert administrative profile/editor settings into
    // JavaScript settings.
    'settings callback' => 'codemirror_editor_settings',
    // Defines the list of supported (minimum) versions of the editor library,
    // and the respective Drupal integration files to load.
    'versions' => array(
      '4.8.0' => array(
        'js files' => array('codemirror.js'),
      ),
    ),
  );

  return $editor;
}

/**
 * Detect editor version.
 *
 * @param $editor
 *   An array containing editor properties as returned from hook_editor().
 *
 * @return
 *   The installed editor version.
 */
function codemirror_editor_version($editor) {
  $contents = file_get_contents($editor['library path'] . '/package.json');
  if ($contents) {
    $json = json_decode($contents);
    if ($json) {
      return $json->version;
    }
  }

  return FALSE;
}

/**
 * Determine available editor themes or check/reset a given one.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $profile
 *   A wysiwyg editor profile.
 *
 * @return
 *   An array of theme names. The first returned name should be the default
 *   theme name.
 */
function codemirror_editor_themes($editor, $profile) {
  $path = $editor['library path'] . '/theme/';

  if (file_exists($path)) {
    $dir_handle = opendir($path);

    if ($dir_handle) {
      $themes = array();
      while ($file = readdir($dir_handle)) {
        if (substr($file, 0, 1) != '.') {
          $themes[] = basename($file, '.css');
        }
      }
      closedir($dir_handle);
      natcasesort($themes);
      $themes = array('default') + array_values($themes);
      return !empty($themes) ? $themes : array('default');
    }
  }
  else {
    return array('default');
  }
}

/**
 * Enhances the editor profile settings form.
 */
function codemirror_editor_settings_form(&$form, &$form_state) {
  $form['buttons']['#access']    = FALSE;
  $form['appearance']['#access'] = FALSE;
  $form['output']['#access']     = FALSE;
  $form['css']['#access']        = FALSE;

  $settings = $form_state['wysiwyg_profile']->settings;

  $modes = array(
    'html'     => t('HTML Mixed'),
    'markdown' => t('Markdown'),
    'php'      => t('PHP'),
  );

  $form['basic']['mode'] = array(
    '#type'          => 'select',
    '#title'         => t('Mode'),
    '#options'       => $modes,
    '#default_value' => !empty($settings['mode']) ?
                          $settings['mode'] : 'html',
  );

  $themes = drupal_map_assoc(codemirror_editor_themes(
    $form_state['wysiwyg']['editor'], $form_state['wysiwyg_profile']));

  $form['basic']['theme'] = array(
    '#type'          => 'select',
    '#title'         => t('Theme'),
    '#options'       => $themes,
    '#default_value' => $settings['theme'],
    '#weight'        => 100,
  );
}

/**
 * Return runtime editor settings for a given wysiwyg profile.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $config
 *   An array containing wysiwyg editor profile settings.
 * @param $theme
 *   The name of a theme/GUI/skin to use.
 *
 * @return
 *   A settings array to be populated in
 *   `Drupal.settings.wysiwyg.configs.{editor}`
 */
function codemirror_editor_settings($editor, $config, $theme) {
  $module_path = drupal_get_path('module', 'codemirror_editor');

  drupal_add_css($editor['library path'] . '/lib/codemirror.css', array(
    'group'  => CSS_THEME,
    'weight' => 0,
  ));

  drupal_add_css($editor['library path'] . '/addon/hint/show-hint.css', array(
    'group'  => CSS_THEME,
    'weight' => 1,
  ));

  drupal_add_css($module_path . '/editors/css/codemirror-custom.css', array(
    'group'  => CSS_THEME,
    'weight' => 2,
  ));

  if ($theme != 'default') {
    drupal_add_css($editor['library path'] . "/theme/{$theme}.css", array(
      'group'  => CSS_THEME,
      'weight' => 3,
    ));
  }

  $settings = array(
    'theme'         => $theme,
    'indentUnit'    => 2,
    'smartIndent'   => TRUE,
    'tabSize'       => 2,
    'identWithTabs' => FALSE,
    'lineWrapping'  => TRUE,
  );

  $settings += call_user_func_array(
    "codemirror_editor_{$config['mode']}_settings",
    array($editor, $config, $theme));

  return $settings;
}

/**
 * HTML profile settings.
 */
function codemirror_editor_html_settings($editor, $config, $theme) {
  return array(
    'mode'              => 'htmlmixed',
    'lineNumbers'       => TRUE,
    'matchBrackets'     => TRUE,
    'autoCloseBrackets' => TRUE,
    'matchTags'         => array('bothTags' => TRUE),
    'autoCloseTags'     => TRUE,
    'showTrailingSpace' => TRUE,
    'styleActiveLine'   => TRUE,
    'extraKeys'         => array('Ctrl-Space' => 'autocomplete'),
  );
}

/**
 * Markdown profile settings.
 */
function codemirror_editor_markdown_settings($editor, $config, $theme) {
  return array(
    'mode'              => 'markdown',
    'matchBrackets'     => TRUE,
    'autoCloseBrackets' => TRUE,
  );
}

/**
 * PHP profile settings.
 */
function codemirror_editor_php_settings($editor, $config, $theme) {
  $html_settings = codemirror_editor_html_settings($editor, $config, $theme);

  return array_merge($html_settings, array(
    'mode' => 'php',
  ));
}
