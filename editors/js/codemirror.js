(function($) {

Drupal.wysiwyg.codeMirrorInstances = {};

/**
 * Attach this editor to a target element.
 */
Drupal.wysiwyg.editor.attach.codemirror = function(context, params, settings) {
  if (typeof params != 'undefined') {
    var field = document.getElementById(params.field);
    var cm = CodeMirror.fromTextArea(field, settings);
    Drupal.wysiwyg.codeMirrorInstances[params.field] = cm;
  }
};

/**
 * Detach a single editor or all editors.
 */
Drupal.wysiwyg.editor.detach.codemirror = function(context, params, trigger) {
  if (typeof params != 'undefined') {
    var cm = Drupal.wysiwyg.codeMirrorInstances[params.field];
    if (cm) {
      cm.toTextArea();
      delete Drupal.wysiwyg.codeMirrorInstances[params.field];
    }
  }
};

})(jQuery);
