CodeMirror Editor
=================

Provides syntax highlighting and tag completion for HTML and Markdown using the CodeMirror editor (http://www.codemirror.net).


Installation
------------

* Install as usual, see https://www.drupal.org/documentation/install/modules-themes/modules-7 for further information.
* Visit /admin/config/content/wysiwyg and follow the instructions for downloading the CodeMirror editor.
* Next, setup your editor profiles, you can choose either HTML Mixed, Markdown or PHP editor modes and also choose the editor theme in the "Basic settings" section of the Wysiwyg profiles.
