CHANGELOG
=========

6 February 2015
---------------

- Fixed issue when using with the Inline Entity Form module which meant that
  any text changed in an editor wouldn't be saved.


23 December 2014
----------------

- Added PHP mode


8 December 2014
---------------

 - Initial commit
